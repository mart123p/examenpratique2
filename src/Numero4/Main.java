package Numero4;

import java.util.Arrays;

/**
 * Created by martinpouliot on 15-12-01.
 */
public class Main {
    public static void main(String args[]){
        int[][] iMatNombre = {{15,15,9},{1,3,88},{1,6,66},{1,12,33},{2,9,11},{2,14,55},{4,5,99},{7,2,44},{12,12,77},{13,1,22}};
        int[][] iMatInit = new int[iMatNombre[0][0]][iMatNombre[0][1]];
        for(int i = 1; i < iMatNombre.length; i++) {
            int iLine = iMatNombre[i][0];
            int iColumn = iMatNombre[i][1];
            int iNumber = iMatNombre[i][2];
            //We add it to the new Matrix

            iMatInit[iLine][iColumn] = iNumber;
        }
        System.out.println(Arrays.deepToString(iMatInit));
    }
}

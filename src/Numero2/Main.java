package Numero2;

import java.util.Arrays;

/**
 * Created by martinpouliot on 15-12-01.
 */
public class Main {
    public static void main(String args[]){
        int iMatInit[][] = {{6,7,8,9,10},{1,2,3,4,5},{6,7,8,9,10},{1,2,3,4,5},{1,2,3,4,5}};
        int iVectResult[] = new int[5];

        //We sum all the data
        for(int i = 0; i < iMatInit.length; i ++){
            int iSum = 0;
            for(int i2 = 0; i2 < iMatInit[i].length; i2++){
                iSum += iMatInit[i][i2];
            }
            iVectResult[i] = iSum;
        }
        //We print out the final result
        System.out.println(Arrays.toString(iVectResult));
    }

}
